package id.iketutg.myflyaway.mapper;

import id.iketutg.myflyaway.infrastructure.entity.Profile;
import id.iketutg.myflyaway.domain.data.ProfileDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProfileMapper {
    ProfileMapper INSTANTCE = Mappers.getMapper(ProfileMapper.class);
    ProfileDto profileToProfileDto(Profile profile);
    Profile profileDtoToProfile(ProfileDto profileDto);
}
