package id.iketutg.myflyaway.infrastructure.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "profile")
@Data
public class Profile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(unique = true)
    @Size(max = 50)
    private String phoneNumber;

    @NotBlank
    @Size(min = 1, max = 100)
    private String fullName;

    @NotBlank
    @Size(min = 1, max = 50)
    private String nickName;

    @NotBlank
    @Size(min = 1, max = 50)
    private String urlPhoto;

    @Size(max = 50)
    private String alamat;

}
