package id.iketutg.myflyaway.infrastructure.adapters;

import id.iketutg.myflyaway.infrastructure.entity.Profile;
import id.iketutg.myflyaway.domain.port.spi.ProfilePersistencePort;
import id.iketutg.myflyaway.repository.ProfileRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Slf4j
@Service
public class ProfileJpaAdapter implements ProfilePersistencePort {

    @Autowired
    ProfileRepository profileRepository;

    @Override
    public Profile getProfileByPhoneNumber(String phoneNumber) {
        Profile profile = profileRepository.findFirstByPhoneNumber(phoneNumber)
                .orElseThrow(NoSuchElementException::new);
        return profile;
    }

    @Override
    public Profile save(Profile profile) {
        return null;
    }
}
