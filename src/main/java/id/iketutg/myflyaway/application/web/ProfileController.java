package id.iketutg.myflyaway.application.web;

import id.iketutg.myflyaway.domain.data.ProfileDto;
import id.iketutg.myflyaway.domain.port.api.ProfileServicePort;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1/account")
public class ProfileController {

    @Autowired
    private ProfileServicePort profileServicePort;

    @GetMapping("/profile/{phoneNumber}")
    @PreAuthorize("hasRole('STAMPUSER')")
    public ResponseEntity<?> getProfile(@PathVariable String phoneNumber){
        ProfileDto profileDto = profileServicePort.getProfileByPhoneNumber(phoneNumber);
        return ResponseEntity.ok().body(profileDto);
    }

    @GetMapping("/hello")
    public String getHello(){
        return "Hello";
    }
}
