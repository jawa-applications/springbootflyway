package id.iketutg.myflyaway.domain.port.api;

import id.iketutg.myflyaway.domain.data.ProfileDto;
import id.iketutg.myflyaway.infrastructure.entity.Profile;

public interface ProfileServicePort {
    ProfileDto getProfileByPhoneNumber(String phoneNumber);
    Profile save(ProfileDto profileDto);
}
