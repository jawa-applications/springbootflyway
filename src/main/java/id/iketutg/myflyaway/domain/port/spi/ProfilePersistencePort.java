package id.iketutg.myflyaway.domain.port.spi;

import id.iketutg.myflyaway.infrastructure.entity.Profile;

public interface ProfilePersistencePort {
    Profile getProfileByPhoneNumber(String phoneNumber);
    Profile save(Profile profile);
}
