package id.iketutg.myflyaway.domain.services;

import id.iketutg.myflyaway.domain.data.ProfileDto;
import id.iketutg.myflyaway.domain.port.api.ProfileServicePort;
import id.iketutg.myflyaway.domain.port.spi.ProfilePersistencePort;
import id.iketutg.myflyaway.infrastructure.entity.Profile;
import id.iketutg.myflyaway.mapper.ProfileMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@AllArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileServicePort {

    private final ProfilePersistencePort profilePersistencePort;

    @Override
    public ProfileDto getProfileByPhoneNumber(String phoneNumber) {
        Profile profile = profilePersistencePort.getProfileByPhoneNumber(phoneNumber);
        return ProfileMapper.INSTANTCE.profileToProfileDto(profile);
    }

    @Override
    public Profile save(ProfileDto profileDto) {
        Profile profile = ProfileMapper.INSTANTCE.profileDtoToProfile(profileDto);
        return profilePersistencePort.save(profile);
    }
}
