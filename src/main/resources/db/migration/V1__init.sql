CREATE TABLE profile (
 id bigint(20) NOT NULL AUTO_INCREMENT,
 alamat varchar(50) DEFAULT NULL,
 full_name varchar(100) DEFAULT NULL,
 nick_name varchar(50) DEFAULT NULL,
 phone_number varchar(50) DEFAULT NULL,
 url_photo varchar(50) DEFAULT NULL,
 PRIMARY KEY (id),
 UNIQUE KEY UK_phonenum (phone_number)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;